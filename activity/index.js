// Quiz:
// 1. How do you create arrays in JS?
// by declaring let students = []
// 2. How do you access the first character of an array?
// by querying students[0]
// 3. How do you access the last character of an array?
// by querying studentLast = student[students.length-1]
// 4. What array method searches for, and returns the index of a given value in an array? This method returns -1 if given value is not found in the array.

// 5. What array method loops over all elements of an array, performing a user-defined function on each iteration?

// 6. What array method creates a new array with elements obtained from a user-defined function?

// 7. What array method checks if all its elements satisfy a given condition?

// 8. What array method checks if at least one of its elements satisfies a given condition?

// 9. True or False: array.splice() modifies a copy of the array, leaving the original unchanged.

// 10.True or False: array.slice() copies elements from original array and returns them as a new array.

// console.log(`Hello!`);

// Activity
let students = ['John', 'Paul', 'George', 'Ringo'];
// let addtl = "Ryan";
function addToEnd(students, name) {
    if (typeof name === "string") {
        students.push(name);
        console.log(students);
    } else {
        console.log("Error - can only add strings to an array");
    }
};

function addToStart(students, name) {
    if (typeof name === "string") {
        students.unshift(name);
        console.log(students);
    } else {
        console.log("Error - can only add strings to an array");
    }
};
