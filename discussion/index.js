// console.log("Hello");
let arrNum = [15, 20, 23, 30, 37];

arrNum.forEach(element => {
    if (element % 5 === 0) {
        console.log(`${element} is not divisible by 5.` )
    } else {
        console.log(false);
    }
});

console.log(Math);
console.log(Math.E);
console.log(Math.PI);
console.log(Math.SQRT2);


// Methods
console.log(Math.round(Math.PI));
console.log(Math.trunc(Math.PI));